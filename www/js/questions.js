const questions = {
	"questions":
	[
		{
			"text": "¿Por qué recomendas Actron?",
			"answers":
			[
				{
					"text": "Porque tiene mejor relación precio/calidad",
					"correct": "yes"
				},
				{
					"text": "Porque es más eficaz",
					"correct": "yes"
				},
				{
					"text": "Porque es más seguro que otros AINES",
					"correct": "yes"
				},
				{
					"text": "Porque es de Bayer",
					"correct": "yes"
				},
				{
					"text": "Porque lo tengo al alcance de la mano.",
					"correct": "yes"
				}
			]
		},
		{
			"text": "¿Por qué NO recomendas Actron?",
			"answers":
			[
				{
					"text": "Porque no tiene la mejor relación precio/calidad",
					"correct": "yes"
				},
				{
					"text": "Porque otras marcas son más eficaces.",
					"correct": "yes"
				},
				{
					"text": "Porque otras marcas son más seguras",
					"correct": "yes"
				},
				{
					"text": "Acuerdos Comerciales",
					"correct": "yes"
				}
			]
		},
		{
			"text": "¿Cuál es la principal indicación de Actron Plus?",
			"answers": [
				{
					"text": "Dolor Muscular",
					"correct": "no"
				},
				{
					"text": "Dolor de Cabeza",
					"correct": "yes"
				},
        {
          "text": "Dolores Artculares",
          "correct": "no"
        }
			]
		},
		{
			"text": "¿En cuánto tiempo comienza a actuar Actron 400?",
			"answers":
			[
				{
					"text": "30 minutos",
					"correct": "yes"
				},
				{
					"text": "45 minutos",
					"correct": "no"
				},
				{
					"text": "1 hora",
					"correct": "no"
				},
				{
					"text": "1 hora y media",
					"correct": "no"
				}
			]
		},
		{
			"text": "¿Cuál es la composición Actron Plus?",
			"answers":
			[
				{
					"text": "Ibuprofeno 400 mg + Cafeina 100 mg",
					"correct": "yes"
				},
				{
					"text": "Ibuprofeno 400 mg + Cafeina 50 mg",
					"correct": "no"
				},
				{
					"text": "Ibuprofeno 600 mg + Cafeina 75 mg",
					"correct": "no"
				},
				{
					"text": "Ibuprofeno 600 mg + Cafeina 100 mg",
					"correct": "no"
				}
			]
		}
		,
		{
			"text": "Cuál de estos AINES a tu percepción es la más segura?",
			"answers":
			[
				{
					"text": "Aspirinas",
					"correct": "yes"
				},
				{
					"text": "Diclofenac",
					"correct": "yes"
				},
				{
					"text": "Ibuprofeno",
					"correct": "yes"
				},
				{
					"text": "Paracetamol",
					"correct": "yes"
				}
			]
		}
	]
}
