// Modal - Open
$('button[data-modal]').on('click', function() {
	var modal = $(this).attr('data-modal');
	$('.modal[data-modal="' + modal + '"]').addClass('fade-in');
});
// Modal - Close
$('.modal').on('click', function(e) {
	var tag = $(e.target);
	if (tag.is(this) || tag.is('.close')) {
		$(this).removeClass('fade-in');
	}
});
