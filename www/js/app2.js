var timer = new Timer();
var timer_total = new Timer();

var recomends ;
var qnumber = 0;
var TOTAL_TIME = 0 ;
var FIRST_PAGE = 1 ;

times = {
  "time0" : "0",
  "time1" : "0",
  "time2" : "0",
  "time3" : "0",
  "time4" : "0",
  "time5" : "0"
} ;

options = {
  "q0" : "0",
  "q1" : "0",
  "q2" : "0",
  "q3" : "0",
  "q4" : "0",
  "q5" : "0",
}

var MAX_SECONDS_AMOUNT = 30;
var TIME_OUT = false ;
(function () {
  //--
  var service = new FarmaciasService();
  var views = new Views() ;
  var points ;

  service.initialize().done(function () {
    if(log){
      console.log("Service initialized");
    }
    // backup() ;
    add_listeners() ;

    //RUTA 0
    router.addRoute(' ', function() {
      if(log){
        console.log("Route added");
      }
      FIRST_PAGE = 1 ;
      views.render_index() ;
    });

    router.addRoute('index', function() {
      if(log){
        console.log("Route added");
      }
      FIRST_PAGE = 1 ;
      views.render_index() ;
    });

    router.addRoute('registration', function() {
      if(log){
        FIRST_PAGE = 0 ;
        console.log("Route registration");
      }
      qnumber = 0 ;
      views.render_registration() ;
    });


    router.addRoute('pregame', function() {
      if(log){
        console.log("pre game");
      }
      views.render_pregame() ;
    });

    //Questions
    router.addRoute('question/:i', function(i) {
      var index = parseInt(i) ;
      if(log){
        console.log("QUESTION NUM:" + index) ;
      }
      views.render_questions(i) ;
      qnumber = index ;
      if(i == 0 || i == 1 )
      {
        timer.start({precision: 'secondTenths'});
        timer_total.start({precision: 'seconds'});
      }
    });

    router.addRoute('video', function() {
      var t ;
      if(TIME_OUT){
        TOTAL_TIME = 1000 ;
      }else{
        for (var i = 0; i < 6; i++) {
          t = parseFloat(times["time" + [i]]) ;
          if(log){ console.log("T " + t) ; }
            TOTAL_TIME = TOTAL_TIME + t ;
        }
      }

      if(log){ console.log("TOTAL TIME: " + TOTAL_TIME) ; }

      points = calculate_points(TOTAL_TIME) ;

      $("#total_points_text").html("+ " + points + "<span>puntos acumulados</span>");
      views.render_video() ;
    });

    router.addRoute('points', function() {
      if(log){ console.log("TIMERS STOPPED"); }
      timer.stop();
      timer_total.stop();

      save_comp_stats() ;
      views.render_points() ;
      backupdata() ;
    });

    router.start();

  });

  function add_listeners() {
    if(log){
      console.log("Add listeners");
    }
    /* --------------------------------- Event Registration -------------------------------- */
    // document.addEventListener("deviceready", backupdata, false);

    document.addEventListener("backbutton", onBackKeyDown, false);

    $('#search_key').on('keyup', findById);
    $('#goto_register_btn').on('click', goto_reg);
    $('#start_play_btn').on('click', goto_question0);
    $('#confirm_form').on('click', save_comp_register);
    $('#upload_btn').on('click', upload_data);
    $('#go_begin_btn').on('click', go_to_begin);

    $('#club_bayer').on({ 'pointerdown' : function(){
        if(FIRST_PAGE){
          clean_competitors_db() ;
        }
      }
    }, {passive: true}, false);

    $('#question_a').on({ 'pointerdown' : function(){
        check_answers('0') ;
      }
    }, {passive: true}, false);

    $('#question_b').on({ 'pointerdown' : function(){
      check_answers('1') ;
      }
    }, {passive: true}, false);

    $('#question_c').on({ 'pointerdown' : function(){
      check_answers('2') ;
      }
    }, {passive: true}, false);

    $('#question_d').on({ 'pointerdown' : function(){
      check_answers('3') ;
      }
    }, {passive: true}, false);

    $('#question_e').on({ 'pointerdown' : function(){
      check_answers('4') ;
      }
    }, {passive: true}, false);

    var vid = document.getElementById("video_src");
    vid.addEventListener("ended", function() {
      location.href = "#points" ;
    });

    timer_total.addEventListener('secondsUpdated', function (e) {
      var time_dom = document.getElementById('seconds');

      if(MAX_SECONDS_AMOUNT >= timer_total.getTotalTimeValues().seconds ){
        time_dom.innerText =  Math.abs(timer_total.getTotalTimeValues().seconds - MAX_SECONDS_AMOUNT) + '';
      }
      else{
        time_dom.innerText = MAX_SECONDS_AMOUNT ;
        TIME_OUT = true ;
        go_to_video() ;
      }
    });
}

function onBackKeyDown() {
}

function clean_competitors_db()
{
  var erase = prompt("Escriba la contraseña para borrar la base de datos. Esto solo debe hacerse una vez antes de comenzar la campaña.");
  if (erase == "actronbayer2018") {
    if(log){
      console.log("SI limpiar db") ;
    }
    service.dropCompetitorTable() ;
    alert("Se han borrado todos los datos") ;
  } else {
    if(log){
      console.log("NO limpiar db") ;
    }
  }
}

function findById() {
  if(log){
    console.log("searching drugstore") ;
  }
  service.findById($('#search_key').val()).done(function (farmacias) {
    $('.pharmacy-info').empty();
    // $('.farmacias-list').empty();
    var e;
    e = farmacias ;
    // console.log(e.did) ;
    if (farmacias) {
      // $('.farmacias-list').append('<li><a href="#farmacias/' + e.eid + '">' + e.nom + ' ' + e.dom + '</a></li>');
      var rtext = "Nombre: " + e.nom + " Dirección: " + e.dom ;
      $('.pharmacy-info').text(rtext) ;
    }
  });
}

function save_comp_register(){
  // clear_q_opt() ;
  var valid = 0;
  var did = $('#search_key').val();
  var name_complete = $('#name').val().split(' ');
  var name = name_complete[0];
  var last_name = name_complete[name_complete.length - 1];
  var dni = $('#dni').val();
  var rol = $('#rol_selector').val();
  var rec = $('#recomends').val();

  recomends = rec ;

  var conditions = $('#conditions').is(':checked');

  var currentdate = new Date();
  var fec =   currentdate.getFullYear() + "-"
                + (currentdate.getMonth()+1) + "-"
                + currentdate.getDate() ;
  var hour =  currentdate.getHours() + ":"
                + currentdate.getMinutes() + ":"
                + currentdate.getSeconds() ;


  if(did && name && dni && conditions){
    valid = 1 ;
    var result = service.register_competitor(did, name, last_name, dni, rol, fec, hour, recomends);
    if(log){
      console.log(result);
    }
    go_to_startgame() ;
  }
  else
  {
    if(!did)
    {
      alert("COMPLETE EL NÚMERO DE FARMACIA") ;
    }
    else if (!name) {
      alert("INGRESE NOMBRE Y APELLIDO DEL PARTICIPANTE") ;
    }
    else if (!dni) {
      alert("INGRESE EL D.N.I. DEL PARTICIPANTE") ;
    }
    else{
      alert("ACEPTE LAS CONDICIONES DE USO PARA PARTICIPAR") ;
    }
  }

  return false ;
  // return valid ;
}

function save_comp_stats()
{
  var did = $('#search_key').val();

  if (log){
    console.log("TOTAL TIME FALSE: " + TOTAL_TIME) ;
    console.log(times) ;
  }

  service.save_comp_stats(did,
     options["q"+0], times.time0,
     options["q"+1], times.time1,
     options["q"+2], times.time2,
     options["q"+3], times.time3,
     options["q"+4], times.time4,
     options["q"+5], times.time5,
     TOTAL_TIME,
     points
  );
}

function upload_data()
{
    service.select_all("competitors").done(function (competitors) {
       //Create array
       var c = [];
       for (var i = 0; i < competitors.length; i++) {
         //Push to array
         c.push(competitors.item(i));
       }
       //stringify
       var comp  = JSON.stringify(c, null, '\t');
       if(log){
         console.log("DATA TO SEND:" + comp);
       }
      send_data("user=pepe" + "&pwd=79714b6b97eef9c1fcb060544d2a2ca4&competitors=" + comp ) ;
    });
  // return false() ;
}

function backupdata() {
  var data ;
  service.select_all("competitors").done(function (competitors) {
     //Create array
     var c = [];
     for (var i = 0; i < competitors.length; i++) {
       //Push to array
       c.push(competitors.item(i));
     }
     //stringify
     data  = JSON.stringify(c, null, '\t');
     if(log){
       // console.log("DATA TO BACKUP:" + data);
     }
  });


  var currentdate = new Date();
  var fec =   currentdate.getFullYear() + "-"
                + (currentdate.getMonth()+1) + "-"
                + currentdate.getDate() ;

  var fileName = 'actron_backup_'+ fec + '.txt';
  window.resolveLocalFileSystemURL( cordova.file.externalRootDirectory, function( directoryEntry ) {
      directoryEntry.getFile(fileName, { create: true }, function( fileEntry ) {
          fileEntry.createWriter( function( fileWriter ) {
              fileWriter.onwriteend = function( result ) {
                  console.log( 'done.' );
              };
              fileWriter.onerror = function( error ) {
                  console.log( error );
              };
              fileWriter.write( data );
          }, function( error ) { console.log( error ); } );
      }, function( error ) { console.log( error ); } );
  }, function( error ) { console.log( error ); } );
}

function backup(){


}

}());
