function Views(){
  console.log("Views Initialized") ;
}

Views.prototype.render_index = function () {
  $("#election").attr('class', 'fade-in');
  $("#questionary").attr('class', '');
  $("#user").attr('class', '');
  $("#video").attr('class', '');
  $("#points").attr('class', '');
};

Views.prototype.render_registration = function () {
  $('#search_key').val("");
  $('#name').val("");
  $('#dni').val("");
  $('#rol_selector').val(1);
  $('#pharmacy-info').text("Info de la Farmacia") ;
  // $('#conditions').is(':checked');

  $("#election").attr('class', '');
  $("#questionary").attr('class', '');
  $("#user").attr('class', 'fade-in');
};

Views.prototype.render_questions = function (num) {
  var paragraph = questions.questions[num].text ;

  var question_a = questions.questions[num].answers[0].text ;
  var question_b = questions.questions[num].answers[1].text ;
  var question_c = questions.questions[num].answers[2].text ;

  if(questions.questions[num].answers.length > 3){
    $("#qlist").attr('class', 'q-4');
    var question_d = questions.questions[num].answers[3].text ;
    $("#q_d").text(question_d) ;
    $("#question_d").show();
  }else{
    $("#question_d").hide();
    $("#qlist").attr('class', '');
  }

  $("#qlist").show();

  $("#q_paragraph").text(paragraph) ;
  $("#q_a").text(question_a) ;
  $("#q_b").text(question_b) ;
  $("#q_c").text(question_c) ;

  $("#pre-questionary").attr('class', '');
  $("#election").attr('class', '');
  $("#user").attr('class', '');
  $("#questionary").attr('class', 'fade-in');

};

Views.prototype.render_video = function(){
  $("#election").attr('class', '');
  $("#questionary").attr('class', '');
  $("#user").attr('class', '');
  $("#video").attr('class', 'fade-in');
  $('#video_src').get(0).play() ;
};

Views.prototype.render_points = function(){
  $("#election").attr('class', '');
  $("#questionary").attr('class', '');
  $("#user").attr('class', '');
  $("#video").attr('class', '');
  $("#points").attr('class', 'fade-in');

  // setTimeout(function(){
  //   go_to_begin();
  // }, 30000);
};

Views.prototype.render_pregame = function(){
  $("#election").attr('class', '');
  $("#questionary").attr('class', '');
  $("#user").attr('class', '');
  $("#video").attr('class', '');
  $("#pre-questionary").attr('class', 'fade-in');
};
