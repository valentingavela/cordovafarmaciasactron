//Check questions
function check_answers(opt) {
  // var qnumber = localStorage.getItem("qnumber");

  if (options["q"+qnumber] == "0") {
    options["q"+qnumber] = parseInt(opt) + 1 ;
  }

  // if(!localStorage.getItem("q" + qnumber + "-opt"))
  // {
  //   localStorage.setItem("q" + qnumber + "-opt", parseInt(opt)+1);
  // }

  alist = ["question_a", "question_b", "question_c", "question_d", "question_e"] ;
  if(log){console.log("option selected " + opt) ;
          console.log("question number " + qnumber) ; }

  var check_selected = questions.questions[qnumber].answers[opt].correct ;

  if ( check_selected == "yes" ){
    if(log){ console.log("Cooorrecto!") ; }
    // SAVE answer time
    times["time" + qnumber] = timer.getTimeValues().seconds + '.' + timer.getTimeValues().secondTenths ;

    $("#"+alist[opt]).attr('class', 'correct'); //marcar verde
    //Pasar a la siguiente pregunta con timeout
    setTimeout(function(){
      goto_next_question() ;
    }, 100);
  }
  else
  {
    $("#answer_error").attr('class', 'alert error fade-in');

    setTimeout(function(){
      $("#answer_error").attr('class', 'alert error');
    }, 1000);
    if(log){ console.log("Incorrecto") ; }
  }
}

function clean_answers() {
  var alist = ["question_a", "question_b", "question_c", "question_d", "question_e"] ;
  var i ;
  for(i=0; i < alist.length; i++)
  {
    $("#"+alist[i]).removeClass();
    if(log){ console.log("#"+alist[i]) ; }
  }
}

function clean_answers_storage(){
  for (var i = 0; i < 6; i++) {
    // localStorage.removeItem("q" + i + "-opt");
    times["time" + i] = "0" ;
    options["q" + i] = "0" ;
  }
}

function calculate_points(seconds){
  var total_points ;
  var nrand = getRandomInt(9);
  seconds = 30.0 - parseFloat(seconds) ;

  if(seconds > 21.0)
  {
    total_points = 4000 - nrand ;
  }
  else if (seconds < 1.0) {
    total_points = 1000 + nrand ;
  }
  else{
    total_points = seconds * 100.0 + 1000.0 + nrand ;
  }
  //
  if(log){
      console.log("TIME CHRONO:" + seconds) ;
      console.log("POINTS! : " + total_points) ;
  }
  return parseInt(total_points) + '' ;
}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}
