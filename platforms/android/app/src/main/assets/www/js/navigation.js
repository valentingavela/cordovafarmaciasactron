// var points = 0 ;

function go_to_begin()
{
  location.href = "#index" ;
}

function goto_reg() {
  TIME_OUT = false ;
  TOTAL_TIME = 0 ;
  if(log){ console.log("goto registration") ; }
  location.href = "#registration" ;
}

//Change location to question0
function goto_question0() {
  clean_answers_storage() ;
  $("#answer_error").attr('class', 'alert error');
  if(log){ console.log("START PLAY") ; }
  if(recomends == 1){
    location.href = "#question/0" ;
  }else {
    location.href = "#question/1" ;
  }
  timer_total.start() ;
}

function goto_next_question(){
  $('#qlist').hide() ;
  clean_answers() ;
  var current_qn  = qnumber ;
  var nq = current_qn + 1 ;

  if(nq < questions.questions.length){

    if(log){ console.log("QNUMBER: " + nq + " QLEN: " + questions.questions.length); }
    if(nq == 1){
      location.href = "#question/" + 2 ;
    }else {
      location.href = "#question/" + nq ;
    }
  }
  else{
    go_to_video() ;
  }
  timer.reset() ;
}

function go_to_video(){
  location.href = "#video";
}

function go_to_startgame(){
  location.href = "#pregame";
}
