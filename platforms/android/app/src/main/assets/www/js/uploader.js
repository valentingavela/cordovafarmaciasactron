function checkConnection() {
    var networkState = navigator.connection.type;
    if(networkState != "none")
    {
      return true ;
    }
    else
    {
      return false ;
    }
}

function send_data(data) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if(!checkConnection())
    {
      alert("NO SE PUDIERON ENVIAR LOS DATOS. CHECKEE SU CONEXIÓN A INTERNET") ;
      return ;
    }
    if(this.readyState == 4 && this.status == 200) {
      alert("DATOS ENVIADOS CON EXITO") ;
      // console.log("respuesta:" + this.responseText) ;
    }
    if(this.readyState == 4 && this.status == 500 || this.status == 404) {
      alert("NO SE PUDIERON ENVIAR LOS DATOS. CHECKEE SU CONEXIÓN A INTERNET") ;
    }
  };
  xhttp.open("POST", "https://benteveo.com/cgi-bin/actron_upload_data.pl", true);
  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(data);
  // xhttp.send("fname=pepe&lname=lepe");
}
