function backup(table) {
	var def = new $.Deferred();
	db.readTransaction(function(tx) {
		tx.executeSql("select * from "+table, [], function(tx,results) {
			var data = convertResults(results);
			console.dir(data);
			def.resolve(data);
		});
	}, dbError);

	return def;
}

$(document).on("click", "#doBackupBtn", function(e) {
	e.preventDefault();
	console.log("Begin backup process");

	$.when(
		backup("competitors"),
		backup("locales")
	).then(function(competitors, locales) {
		console.log("All done");
		//Convert to JSON
		var data = {competitors:competitors, locales:locales};
		var serializedData = JSON.stringify(data);
		console.log(serializedData);
	});

});
